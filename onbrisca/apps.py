from django.apps import AppConfig


# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
class OnbriscaConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "onbrisca"
