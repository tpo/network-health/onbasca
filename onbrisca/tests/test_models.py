# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
from django.forms.models import model_to_dict
from django.test import TestCase

from onbrisca.factories import BridgeFactory, BridgeMeasurementFactory
from onbrisca.models import Bridge, BridgeMeasurement


class TestCaseBridge(TestCase):
    def test_create(self):
        """
        Test the creation of a Bridge model using a factory
        """
        _ = BridgeFactory.create()
        self.assertEqual(Bridge.objects.count(), 1)

    def test_create_batch(self):
        """
        Test the creation of 5 Bridge models using a factory
        """
        bridges = BridgeFactory.create_batch(5)
        self.assertEqual(Bridge.objects.count(), 5)
        self.assertEqual(len(bridges), 5)

    def test_attribute_count(self):
        """
        Test that all attributes of Bridge server are counted.
        It will count the primary key and all editable attributes.
        This test should break if a new attribute is added.
        """
        bridge = BridgeFactory.create()
        bridge_dict = model_to_dict(bridge)
        self.assertEqual(len(bridge_dict.keys()), 8)

    def test_attribute_content(self):
        """
        Test that all attributes of Bridge server have content.
        This test will break if an attributes name is changed.
        """
        bridge = BridgeFactory.create()
        self.assertIsNotNone(bridge._obj_created_at)
        self.assertIsNotNone(bridge._obj_updated_at)
        self.assertIsNotNone(bridge.fingerprint)
        self.assertIsNotNone(bridge.bridgeline)
        self.assertIsNotNone(bridge._bw_mean)
        self.assertIsNotNone(bridge._ratio_stream)


class TestCaseBridgeMeasurement(TestCase):
    def test_create(self):
        """
        Test the creation of a BridgeMeasurement model using a factory
        """
        _ = BridgeMeasurementFactory.create()
        self.assertEqual(BridgeMeasurement.objects.count(), 1)

    def test_create_batch(self):
        """
        Test the creation of 5 BridgeMeasurement models using a factory
        """
        bridge_measurements = BridgeMeasurementFactory.create_batch(5)
        self.assertEqual(BridgeMeasurement.objects.count(), 5)
        self.assertEqual(len(bridge_measurements), 5)

    def test_attribute_count(self):
        """
        Test that all attributes of BridgeMeasurement server are counted.
        It will count the primary key and all editable attributes.
        This test should break if a new attribute is added.
        """
        bridge_measurement = BridgeMeasurementFactory.create()
        bridge_measurement_dict = model_to_dict(bridge_measurement)
        self.assertEqual(len(bridge_measurement_dict.keys()), 5)

    def test_attribute_content(self):
        """
        Test that all attributes of BridgeMeasurement server have content.
        This test will break if an attributes name is changed.
        """
        bridge_measurement = BridgeMeasurementFactory.create()
        self.assertIsNotNone(bridge_measurement.id)
        self.assertIsNotNone(bridge_measurement._obj_created_at)
        self.assertIsNotNone(bridge_measurement._obj_updated_at)
        self.assertIsNotNone(bridge_measurement.bridge)
        self.assertIsNotNone(bridge_measurement.webserver)
        self.assertIsNotNone(bridge_measurement.bandwidth)
        self.assertIsNotNone(bridge_measurement.error)
