# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
from onbrisca import defaults
from onbrisca._config import Config

config = Config(defaults)
