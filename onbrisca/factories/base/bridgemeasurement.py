# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Generated using factory_generator 1.0.4.
"""

import factory

from onbrisca.models.bridge_measurement import BridgeMeasurement


class BridgeMeasurementFactoryBase(factory.django.DjangoModelFactory):
    bridge = factory.SubFactory(factory="onbrisca.factories.BridgeFactory")
    webserver = factory.SubFactory(
        factory="onbasca.onbasca.factories.WebServerFactory"
    )
    bandwidth = factory.Faker(provider="random_int", min=0, max=2147483647)
    error = factory.Faker(provider="text")

    class Meta:
        model = BridgeMeasurement
