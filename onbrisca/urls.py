# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.urls import path

from . import config, views

urlpatterns = [
    path(config.API_PATH, views.create_bridges, name="create_bridges"),
]
