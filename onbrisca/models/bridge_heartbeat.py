# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import datetime
import logging
import time

from onbasca.base.models.base import BaseManager
from onbasca.onbasca.models.heartbeat import Heartbeat
from onbrisca import config
from onbrisca.models.bridge import Bridge
from onbrisca.models.bridge_measurement import BridgeMeasurement

logger = logging.getLogger(__name__)


class BridgeHeartbeatManager(BaseManager):
    pass


class BridgeHeartbeat(Heartbeat):
    class Meta:
        proxy = True

    objects = BridgeHeartbeatManager()

    def log_status(self):
        """
        Print the new percentage of the different bridges that were measured.

        This way it can be known whether the scanner is making progress
        measuring all the Network.

        Log the percentage, the number of bridges measured and not measured,
        the number of loops and the time elapsed since it started measuring.
        """
        self.measured_count = (
            BridgeMeasurement.objects.filter(
                _obj_created_at__gte=self._obj_created_at
            )
            .distinct("bridge__fingerprint")
            .count()
        )
        not_measured_count = Bridge.objects.filter(
            measurements__isnull=True
        ).count()

        self.elapsed_time = self._obj_updated_at - datetime.datetime.utcnow()
        elapsed_hours = self.elapsed_time.total_seconds() / 60 / 60

        self.previous_measured_percent = self.measured_percent
        self.measured_percent = (
            self.measured_count
            # Avoid division by 0
            / (Bridge.objects.count() or 1)
            * 100
        )
        self.save()
        logger.info("Run %s main loops.", self.loops_count)
        logger.info(
            "Measured in total %s (%s%%) unique bridges in %.2f hours",
            self.measured_count,
            self.measured_percent,
            elapsed_hours,
        )
        logger.info("%s bridges still not measured.", not_measured_count)

        # The case when it is equal will only happen when all the bridges
        # have been measured.
        if self.measured_percent <= self.previous_measured_percent:
            logger.warning(
                "There is no progress measuring new unique bridges."
            )
        if self.elapsed_time.total_seconds() < config.BRIDGESCAN_DURATION:
            sleep_seconds = (
                config.BRIDGESCAN_DURATION - self.elapsed_time.total_seconds()
            )
            logger.info("Sleeping for %s secs.", sleep_seconds)
            time.sleep(sleep_seconds)
