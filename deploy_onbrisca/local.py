# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""Django development settings for onbriscapr project."""
from os import environ

# The ``SECRET_KEY`` can be generated with the following commands:
# from django.core.management.utils import get_random_secret_key  # noqa: E402
# SECRET_KEY = get_random_secret_key()
SECRET_KEY = environ.get("SECRET_KEY", "django-insecure-c7koj=4z7wh0g--^h@8_#e34dabvzr&5*rlp9mttkw@$bi*^0w")

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": environ.get("POSTGRES_DB", "onbasca"),
        "USER": environ.get("POSTGRES_USER", "onbasca"),
        "PASSWORD": environ.get("POSTGRES_PASSWORD", "onbasca"),
        "HOST": environ.get("POSTGRES_HOST", "localhost"),
        "PORT": environ.get("POSTGRES_PORT", 5432),
    }
}

# IMPORTANT: change this in `onbriscapr/settrings/local.py`
API_TOKEN_VALUE = "CHANGEME"
