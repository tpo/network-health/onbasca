#!/bin/bash
# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: CC0-1.0

# Quick and dirty script to install `onbrisca` dependencies, create DB and user
#in Debian stable
set -e

export LANG=en_US.UTF-8
USER=${USER-onbasca}
POSTGRES_DB=${POSTGRES_DB-onbasca}
POSTGRES_USER=${POSTGRES_USER-onbasca}
POSTGRES_PASSWORD=${POSTGRES_PASSWORD-onbasca}

FULL_PATH=$(realpath $0)
DIR_PATH=$(dirname $FULL_PATH)
ROOT_PATH=$(dirname $DIR_PATH )

echo "Installing system dependencies"
apt update
apt install -y curl tor postgresql postgresql-contrib virtualenv systemd
# These ^ will also install:
#   build-essential cron dbus dmsetup exim4-base exim4-config exim4-daemon-light gsasl-common guile-2.2-libs javascript-common libapparmor1 libargon2-1
#   libc-l10n libcap2 libcryptsetup12 libdbus-1-3 libdevmapper1.02.1 libgc1 libgnutls-dane0 libgsasl7 libidn11 libip4tc2 libjs-jquery libjs-sphinxdoc
#   libjs-underscore libjson-c5 libkmod2 libllvm11 libmailutils7 libntlm0 libpopt0 libpython3-dev libpython3.9 libpython3.9-dev libsensors-config libsensors5
#   libunbound8 libz3-4 locales logrotate mailutils mailutils-common postgresql-13 postgresql-client-13 postgresql-client-common postgresql-common psmisc
#   python-pip-whl python3-appdirs python3-dev python3-distlib python3-filelock python3-importlib-metadata python3-more-itertools python3-pip
#   python3-pkg-resources python3-setuptools python3-six python3-virtualenv python3-wheel python3-zipp python3.9-dev runit-helper ssl-cert sysstat
#   systemd-timesyncd tor-geoipdb torsocks

echo "Installing python system dependencies"
apt install -y python3-stem python3-aiohttp-socks python3-requests \
    python3-toml python3-colorlog python3-psycopg2 python3-gunicorn gunicorn
# These ^ will also install:
#   python3-aiohttp python3-aiohttp-socks python3-asgiref python3-async-timeout python3-attr python3-certifi python3-chardet python3-colorlog python3-gunicorn
#   python3-idna python3-multidict python3-psycopg2 python3-requests python3-socks python3-sqlparse python3-stem python3-toml python3-typing-extensions
#   python3-urllib3 python3-yarl

echo "Starting and configuring postgres"
pg_ctlcluster 15 main start
su -c "psql -c \"create role $POSTGRES_USER with login  password '$POSTGRES_PASSWORD'"\" postgres
su -c "createdb -O $POSTGRES_USER $POSTGRES_DB" postgres

echo "Creating local user.."
useradd --create-home --shell /bin/bash "$USER"
# passwd -d "$USER"
USER_HOME=$(getent passwd "$USER" | cut -d: -f6)

sudo loginctl enable-linger $USER

echo "Copying code from $ROOT_PATH to "$USER_HOME"/onbasca"
cp -af $ROOT_PATH "$USER_HOME"/onbasca
chown -R "$USER":"$USER" "$USER_HOME"

echo "Now you can run "$USER_HOME"/onbasca/deploy_onbrisca/install_onbrisca.sh as user $USER"
