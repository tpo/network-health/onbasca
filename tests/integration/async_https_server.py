#! /usr/bin/env python3

# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import argparse
import logging
import os
import random
import ssl

from aiohttp import web

from onbasca.onbasca import constants

HERE_PATH = os.path.dirname(os.path.abspath(__file__))
CERT_PATH = os.path.join(HERE_PATH, "data")
RESPONSE_HEADERS = {"Content-Type": "application/octet-stream"}
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(name=__name__)


async def handle(request):
    logger.debug("Request headers: %s.", request.headers)
    if request.method == "GET":
        logger.info("GET request with http_range: %s.", request.http_range)
        if (
            request.http_range.start is not None
            and request.http_range.stop is not None
        ):
            length = request.http_range.stop - request.http_range.start
            # From python 3.9 replace by random.randbytes.
            random_bytes = bytes(
                [random.randrange(0, 256) for _ in range(0, length)]
            )
            return web.Response(
                body=random_bytes, content_type="application/octet-stream"
            )
        return web.Response(body=None, content_type="application/octet-stream")
    if request.method == "HEAD":
        logger.info("HEAD request.")
        return web.Response(
            body=None,
            content_type="application/octet-stream",
            headers={"Content-Length": str(constants.GIB)},
        )


def main():
    parser = argparse.ArgumentParser(description="aiohttp server example")
    parser.add_argument("-l", "--host", default="127.0.0.1")
    parser.add_argument("-p", "--port", default=28888)
    parser.add_argument(
        "-k", "--certkey", default=os.path.join(CERT_PATH, "localhost.key")
    )
    parser.add_argument(
        "-c", "--certfile", default=os.path.join(CERT_PATH, "localhost.crt")
    )
    args = parser.parse_args()

    app = web.Application()
    app.add_routes(
        [
            web.get("/", handle),
        ]
    )

    ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ssl_context.load_cert_chain(args.certfile, args.certkey)

    web.run_app(app, host=args.host, port=args.port, ssl_context=ssl_context)


if __name__ == "__main__":
    main()
