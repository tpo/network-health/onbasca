#! /usr/bin/env python

# SPDX-FileCopyrightText: 2023 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""Delete objects older than <days>."""
import argparse
import logging
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "onbriscapr.settings")
# Initialize the app
import django  # noqa: E402

if not hasattr(django, 'apps'):
    django.setup()

from onbrisca.management.commands.bridgeclean import Command

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    cmd = Command()
    cmd.add_arguments(parser)
    args = parser.parse_args()
    cmd.handle(**args.__dict__)


if __name__ == "__main__":
    main()
