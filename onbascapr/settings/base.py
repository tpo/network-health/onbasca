# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""Django settings for onbascapr project."""
# flake8: noqa: E501
# E501 line too long
import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "onbasca.onbasca.apps.OnBaScaConfig",
    "onbasca.base.apps.BaseConfig",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "onbascapr.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

WSGI_APPLICATION = "onbascapr.wsgi.application"


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    "default": {
        # django-test-tools  # error installing cause not well packaged
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = "/static/"

DATA_UPLOAD_MAX_NUMBER_FIELDS = 10000

# Custom logging config
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "nocolor": {
            "format": "{asctime} {module}[{process}]: <{levelname}> "
            + "({threadName}) {filename}:{lineno} - {funcName} - {message}",
            "style": "{",
            "datefmt": "%b %d %H:%M:%S",
        },
        "color": {
            "()": "colorlog.ColoredFormatter",
            "log_colors": {
                "DEBUG": "green",
                "INFO": "blue",
                "WARNING": "bold_yellow",
                "ERROR": "bold_red",
                "CRITICAL": "bold_purple",
            },
            "format": "{log_color}{asctime} {module}[{process}]: <{levelname}> "
            + "({threadName}) {filename}:{lineno} - {funcName} - {message}",
            "style": "{",
            "datefmt": "%b %d %H:%M:%S",
        },
    },
    "filters": {
        "require_debug_true": {"()": "django.utils.log.RequireDebugTrue"}
    },
    "handlers": {
        "console": {
            # "filters": ["require_debug_true"],
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "color",
        },
        "file": {
            "level": "DEBUG",
            "class": "logging.handlers.TimedRotatingFileHandler",
            "filename": "debug.log",
            "when": "midnight",
            "utc": True,
            "backupCount": 84,  # 3 times 28 days
            "formatter": "nocolor",
        },
    },
    "loggers": {
        "onbasca": {
            "level": "DEBUG",
            "handlers": ["console", "file"],
            "propagate": True,
        },
        "stem": {
            "level": "DEBUG",
            "handlers": ["console", "file"],
            "propagate": True,
        },
        # "scan": {
        #     "level": "DEBUG",
        #     "handlers": ["console", "file"],
        #     "propagate": True,
        # },
        # "generate": {
        #     "level": "DEBUG",
        #     "handlers": ["console", "file"],
        #     "propagate": True,
        # },
        # "__name__": {
        #     "level": "DEBUG",
        #     "handlers": ["console", "file"],
        #     "propagate": True,
        # },
    },
}
