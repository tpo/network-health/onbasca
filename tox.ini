; SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
;
; SPDX-License-Identifier: CC0-1.0

[tox]
skip_missing_interpreters = True
envlist =
    py{37, 38, 39, 310, 311}
    inst,
    black,
    isort,
    flake8,
    codespell,
    checkmanifest,
    reuse,
    doc,
    ; stats
    integration

[testenv]
; Installing with deps errors with versioneer
; deps = .[test]
skip_install = True
commands =
    pip install .[test]
    ; Use python -m to use the binary from the virtualenv if it is also
    ; installed in the system. Do not use {envbindir} since it would require
    ; to install it in tox virtualenv
    python -m coverage run --append --module pytest -svv

# test that it can be installed with custom commands and clean env
[testenv:inst]
ignore_errors = True
recreate = True

[testenv:integration]
ignore_errors = True
deps = .[test]
allowlist_externals =
    bash
commands =
    bash -c tests/integration/run.sh {envtmpdir}/chutney

[testenv:black]
skip_install = True
deps =
  black
commands =
  black --check --diff onbasca onbrisca tests

[testenv:isort]
skip_install = True
deps =
  isort
commands =
  isort --check-only --diff onbasca onbrisca tests

[testenv:flake8]
skip_install = True
deps = flake8-docstrings
commands =
  flake8 onbasca onbrisca tests

[testenv:codespell]
skip_install = True
deps = codespell
commands =
  codespell onbasca onbrisca tests docs

[testenv:checkmanifest]
skip_install = True
deps = check-manifest
commands =
  check-manifest

[testenv:reuse]
skip_install = True
deps = reuse
commands =
  reuse lint

[testenv:clean]
skip_install = True
deps = coverage
changedir = {toxinidir}
commands = python -m coverage erase

[testenv:stats]
skip_install = True
deps = coverage
commands=
    ; nothing to combine while not using several python versions
    ; python -m coverage combine
    python -m coverage report
    python -m coverage html

[testenv:doc]
deps = .[doc]
allowlist_externals = make
changedir = docs
commands =
    make html
    ; this requires build the pdf images
    ; make latexpdf
    ; make man

; this requires Internet, it should not be in envlist
[testenv:doclinks]
deps = .[doc]
allowlist_externals = make
changedir = docs
commands =
    make linkcheck

; Not included in the envlist, useful to run sometimes.
[testenv:bandit]
deps = bandit
;  --skip B101  ; B101: assert_used
commands = bandit -r --exclude onbasca/_version.py onbasca/ test

; Requires Internet
[testenv:safety]
deps = safety
commands = safety check
