# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
import logging

from django.db import models

from onbasca import util

from .. import constants

logger = logging.getLogger(__name__)


class BaseManager(models.Manager):
    def delete_old(self, days=constants.OLDEST_DATA_DAYS):
        util.delete_old(self, self.model, days)


class BaseModel(models.Model):
    class Meta:
        abstract = True

    _obj_created_at = models.DateTimeField(auto_now_add=True, null=True)
    _obj_updated_at = models.DateTimeField(auto_now=True, null=True)
    objects = BaseManager()
