# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import random

import pytest

from onbasca.onbasca.models.relaydesc import RelayDesc


@pytest.mark.unit_test
@pytest.mark.django_db
def test_flowctrl_2(server_descriptors):
    server_descriptor = random.choice(server_descriptors)
    rd = RelayDesc.objects.from_relay_desc(server_descriptor)
    assert rd
    # We know the cached-descriptors file was obtained from tor
    # version >= 0.4.7.4-alpha-dev and the descriptors have `2` in `FlowCtrl`.
    assert rd._flowctrl_2
