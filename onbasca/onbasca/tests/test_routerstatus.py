# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import random

import pytest

from onbasca.onbasca.models.relaydesc import RelayDesc


@pytest.mark.unit_test
@pytest.mark.django_db
def test_cc_path(torcontrol):
    # TorControl is needed to initialize Consensus with its RouterStatuses and
    # Relays with RelayDescs to obtain the needed object attributes.
    torcontrol.obtain_relays()
    assert torcontrol.consensus._cc_alg_2
    assert torcontrol.consensus._bwscanner_cc_gte_1
    rs = random.choice(torcontrol.consensus.routerstatus_set.all())
    path = rs.helper_path()
    assert path
    assert RelayDesc.objects.get(fingerprint=path[1])._flowctrl_2
