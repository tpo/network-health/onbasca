# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import os.path
from unittest import mock

import pytest
from django.conf import settings
from stem import descriptor

from onbasca.onbasca.torcontrol import TorControl


@pytest.fixture(scope="session")
def tests_data_path():
    # BASE_DIR is the project dir, no the root dir.
    path = os.path.join(settings.BASE_DIR, "..", "tests", "data")
    return path


@pytest.fixture(scope="session")
def router_statuses(tests_data_path):
    path = os.path.join(tests_data_path, "20220401_chutney_cached-consensus")
    rss = descriptor.parse_file(
        path, descriptor_type="network-status-consensus-3 1.0"
    )
    rss_list = list(rss)
    return rss_list


@pytest.fixture(scope="session")
def server_descriptors(tests_data_path):
    sds_list = []
    for filename in [
        "20220401_chutney_cached-descriptors",
        "20220401_chutney_cached-descriptors.new",
    ]:
        path = os.path.join(tests_data_path, filename)
        sds = descriptor.parse_file(
            path, descriptor_type="server-descriptor 1.0"
        )
        sds_list.extend(sds)
    return sds_list


@pytest.fixture(scope="session")
def controller(router_statuses, server_descriptors):
    controller = mock.Mock()
    controller.get_info.return_value = "params cc_alg=2 bwscanner_cc=1"
    controller.get_network_statuses.return_value = router_statuses
    controller.get_server_descriptors.return_value = server_descriptors
    return controller


@pytest.fixture(scope="session")
def torcontrol(controller):
    tc = TorControl(controller=controller)
    tc.testing_network = True
    return tc
