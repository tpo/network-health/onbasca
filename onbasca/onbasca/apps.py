# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.apps import AppConfig


class OnBaScaConfig(AppConfig):
    name = "onbasca.onbasca"
    default_auto_field = "django.db.models.BigAutoField"
