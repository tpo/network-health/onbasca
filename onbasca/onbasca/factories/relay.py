# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Generated using factory_generator 1.0.4.
"""

from .base.relay import RelayFactoryBase


class RelayFactory(RelayFactoryBase):
    """
    Factory class for Relay
    """
