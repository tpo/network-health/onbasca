# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Generated using factory_generator 1.0.4.
"""

import factory
from django.utils import timezone

from onbasca.onbasca.models.scanner import Scanner


class ScannerFactoryBase(factory.django.DjangoModelFactory):
    started_at = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    elapsed_time = factory.Faker(provider="time_delta", end_datetime=None)
    uuid = factory.Faker(provider="uuid4")
    nickname = factory.Faker(provider="pystr", max_chars=64)
    tor_version = factory.Faker(provider="pystr", max_chars=64)
    heartbeat = factory.SubFactory(
        factory="onbasca.onbasca.factories.HeartbeatFactory"
    )

    class Meta:
        model = Scanner
        django_get_or_create = (
            "uuid",
            "heartbeat",
        )
