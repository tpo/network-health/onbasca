# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Generated using factory_generator 1.0.4.
"""

import factory
from django.utils import timezone

from onbasca.onbasca.models.bwfile import BwFile


class BwFileFactoryBase(factory.django.DjangoModelFactory):
    file_created = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    latest_bandwidth = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    scanner_country = factory.Faker(provider="pystr", max_chars=2)
    software = factory.Faker(provider="pystr", max_chars=64)
    software_version = factory.Faker(provider="pystr", max_chars=32)
    tor_version = factory.Faker(provider="pystr", max_chars=64)
    consensus = factory.SubFactory(
        factory="onbasca.onbasca.factories.ConsensusFactory"
    )
    destinations_countries = factory.Faker(provider="pystr", max_chars=64)
    earliest_bandwidth = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    generator_started = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    minimum_number_eligible_relays = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    minimum_percent_eligible_relays = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    number_consensus_relays = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    number_eligible_relays = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    percent_eligible_relays = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    recent_consensus_count = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    recent_priority_list_count = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    recent_priority_relay_count = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    recent_measurement_attempt_count = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    recent_measurement_failure_count = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    recent_measurements_excluded_error_count = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    recent_measurements_excluded_near_count = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    recent_measurements_excluded_few_count = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    recent_measurements_excluded_old_count = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    time_to_report_half_network = factory.Faker(
        provider="time_delta", end_datetime=None
    )
    version = factory.Faker(provider="pystr", max_chars=16)
    _mu = factory.Faker(
        provider="pyfloat", left_digits=None, right_digits=None, positive=None
    )
    _muf = factory.Faker(
        provider="pyfloat", left_digits=None, right_digits=None, positive=None
    )
    _bw_sum = factory.Faker(
        provider="random_int", min=0, max=9223372036854775807
    )
    _bw_scaled_sum = factory.Faker(
        provider="random_int", min=0, max=9223372036854775807
    )
    _limit = factory.Faker(
        provider="random_int", min=0, max=9223372036854775807
    )
    _bw_scaled_limited_sum = factory.Faker(
        provider="random_int", min=0, max=9223372036854775807
    )
    _bw_scaled_limited_rounded_sum = factory.Faker(
        provider="random_int", min=0, max=9223372036854775807
    )
    _heartbeat = factory.SubFactory(
        factory="onbasca.onbasca.factories.HeartbeatFactory"
    )

    class Meta:
        model = BwFile
        django_get_or_create = ("file_created",)
