# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Generated using factory_generator 1.0.4.
"""

import factory

from onbasca.onbasca.models.relay import Relay


class RelayFactoryBase(factory.django.DjangoModelFactory):
    fingerprint = factory.Faker(provider="pystr", max_chars=40)
    _routerstatuses_bandwidth_mean = factory.Faker(
        provider="random_int", min=0, max=2147483647
    )
    _relaydescs_min_bandwidth_mean = factory.Faker(
        provider="random_int", min=0, max=2147483647
    )
    _dead = factory.Faker(provider="random_element", elements=(True, False))
    _bw_mean = factory.Faker(provider="random_int", min=0, max=2147483647)
    _bw_median = factory.Faker(provider="random_int", min=0, max=2147483647)
    _bw_filt = factory.Faker(provider="random_int", min=0, max=2147483647)
    _ratio_stream = factory.Faker(
        provider="pyfloat", left_digits=None, right_digits=None, positive=None
    )
    _ratio_filt = factory.Faker(
        provider="pyfloat", left_digits=None, right_digits=None, positive=None
    )
    _ratio = factory.Faker(
        provider="pyfloat", left_digits=None, right_digits=None, positive=None
    )
    _bw_scaled = factory.Faker(
        provider="pyfloat", left_digits=None, right_digits=None, positive=None
    )
    # `_measurement_latest` would raise max recursion
    # _measurement_latest = factory.SubFactory(
    #     factory="onbasca.onbasca.factories.MeasurementFactory"
    # )

    @factory.post_generation
    def consensuses(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of consensuses were passed in, use them
            for item in extracted:
                self.consensuses.add(item)

    class Meta:
        model = Relay
        django_get_or_create = (
            "fingerprint",
            # "_measurement_latest",
        )
