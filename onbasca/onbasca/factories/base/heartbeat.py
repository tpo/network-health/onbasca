# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Generated using factory_generator 1.0.4.
"""

import factory
from django.utils import timezone

from onbasca.onbasca.models.heartbeat import Heartbeat


class HeartbeatFactoryBase(factory.django.DjangoModelFactory):
    loops_count = factory.Faker(
        provider="random_int",
        min=-9223372036854775808,
        max=9223372036854775807,
    )
    measured_count = factory.Faker(
        provider="random_int",
        min=-9223372036854775808,
        max=9223372036854775807,
    )
    measured_percent = factory.Faker(
        provider="pyfloat", left_digits=None, right_digits=None, positive=None
    )
    previous_measured_percent = factory.Faker(
        provider="pyfloat", left_digits=None, right_digits=None, positive=None
    )
    elapsed_time = factory.Faker(provider="time_delta", end_datetime=None)
    _min_percent_relays_to_report_reached_at = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )

    class Meta:
        model = Heartbeat
