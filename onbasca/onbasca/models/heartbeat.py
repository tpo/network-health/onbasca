# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import datetime
import logging

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from onbasca.base.models.base import BaseModel
from onbasca.onbasca import config
from onbasca.onbasca.models.consensus import Consensus
from onbasca.onbasca.models.relay import Relay

logger = logging.getLogger(__name__)


class Heartbeat(BaseModel):
    class Meta:
        get_latest_by = "_obj_updated_at"

    # There is only one Heartbeat per Scanner, but a new one is created every
    # time the Scanner starts to have the history of the measured relays in
    # the past.
    loops_count = models.BigIntegerField(default=0)
    # self.measured_fp_set = set()
    # self.consensus_fp_set = set()
    measured_count = models.BigIntegerField(default=0)
    measured_percent = models.FloatField(default=0)
    previous_measured_percent = models.FloatField(default=0)
    elapsed_time = models.DurationField(default=datetime.timedelta())
    _min_percent_relays_to_report_reached_at = models.DateTimeField(
        blank=True, null=True
    )

    def __str__(self):
        return "{}".format(self._obj_created_at)

    def increment_loops(self):
        self.loops_count += 1
        self.save()

    def log_status(self):
        """Print the new percentage of the different relays that were measured.

        This way it can be known whether the scanner is making progress
        measuring all the Network.

        Log the percentage, the number of relays measured and not measured,
        the number of loops and the time elapsed since it started measuring.
        """
        self.measured_count = (
            Relay.objects.filter(
                measurements__attempted_at__gte=self._obj_created_at
            )
            .distinct("fingerprint")
            .count()
        )
        # not_measured_count =

        self.elapsed_time = self._obj_updated_at - datetime.datetime.utcnow()
        elapsed_hours = self.elapsed_time.total_seconds() / 60 / 60

        self.previous_measured_percent = self.measured_percent
        self.measured_percent = (
            self.measured_count
            # Avoid division by 0
            / (Consensus.objects.latest().relay_set.count() or 1)
            * 100
        )
        self.save()
        logger.info("Run %s main loops.", self.loops_count)
        logger.info(
            "Measured in total %s (%s%%) unique relays in %.2f hours",
            self.measured_count,
            self.measured_percent,
            elapsed_hours,
        )
        # logger.info("%s relays still not measured.", len(not_measured_count))

        # The case when it is equal will only happen when all the relays
        # have been measured.
        if self.measured_percent <= self.previous_measured_percent:
            logger.warning("There is no progress measuring new unique relays.")


@receiver(post_save, sender=Heartbeat, dispatch_uid="delete_old")
def delete_old(sender, instance, **kwargs):
    old = datetime.datetime.utcnow() - datetime.timedelta(
        days=config.OLDEST_DATA_DAYS
    )
    for obj in sender.objects.filter(_obj_created_at__lt=old):
        logger.debug("Deleting old Heartbeat %s", obj)
        obj.delete()
