# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import logging

from django.db import models

from onbasca import util
from onbasca.base.models.relaydesc import RelayDescBase, RelayDescManagerBase
from onbasca.onbasca import constants

from .relay import Relay

logger = logging.getLogger(__name__)


class RelayDescManager(RelayDescManagerBase):
    def from_relay_desc(self, relay_desc):
        relay, _ = Relay.objects.get_or_create(
            fingerprint=relay_desc.fingerprint
        )
        kwargs = {
            "relay": relay,
            "_hibernating": relay_desc.hibernating,
        }
        # NOTE: stem doesn't seem to obtain `pr`` nor create `protocols``
        # protocols = self._from_ns("protocols")
        protocols = relay_desc.protocols
        if protocols:
            if 2 in protocols.get("FlowCtrl", []):
                # logger.debug("Exit with 2 in `FlowCtrl`.")
                kwargs["_flowctrl_2"] = True
        else:
            logger.debug("Exit without 2 in `FlowCtrl`.")
        kwargs_overload = {}
        for key, attr in constants.DESC_OVERLOAD_KEY_ATTRS.items():
            if relay_desc._entries.get(key, None):
                kwargs_overload[attr] = relay_desc._entries[key][0][0][2:]
        # if kwargs_overload:
        #     logger.info(
        #         "%s (%s) is overloaded: %s",
        #         relay_desc.fingerprint,
        #         relay_desc.nickname,
        #         kwargs_overload,
        #     )
        kwargs.update(kwargs_overload)
        rd = super().from_relay_desc(relay_desc, **kwargs)
        return rd

    def delete_old(self):
        from onbrisca import config

        if config.BRIDGESCAN:
            days = config.BRIDGE_OLDEST_DATA_DAYS
        else:
            from onbasca.onbasca import config

            days = config.OLDEST_DATA_DAYS
        util.delete_old(self, RelayDesc, days)


class RelayDesc(RelayDescBase):
    objects = RelayDescManager()
    relay = models.ForeignKey(
        "Relay", on_delete=models.CASCADE, null=True, blank=True
    )
    _hibernating = models.BooleanField(null=True, blank=True)
    overload_general = models.DateTimeField(null=True, blank=True)
    overload_ratelimits = models.DateTimeField(null=True, blank=True)
    overload_fd_exhausted = models.DateTimeField(null=True, blank=True)
    _flowctrl_2 = models.BooleanField(null=True, blank=True)

    def __str__(self):
        return str(self.relay)
