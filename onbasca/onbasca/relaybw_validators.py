# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""RelayBw validators.

Validators to ensure the fields of a RelayBw have values within the correct
range.

"""
import logging

from onbasca.onbasca import config, util

logger = logging.getLogger(__name__)


def validate_relay_in_recent_consensus_count_max(value):
    key = "relay_in_recent_consensus_count"
    if value > config.MAX_RECENT_CONSENSUS_COUNT:
        logger.warning(
            "%s (%s) is greater than the maximum.",
            key,
            value,
        )


def validate_relay_recent_priority_list_count_max(value):
    key = "relay_recent_priority_list_count"
    if value > config.MAX_RECENT_PRIORITY_LIST_COUNT:
        logger.warning(
            "%s (%s) is greater than the maximum.",
            key,
            value,
        )


def validate_relay_recent_measurement_attempt_count_max(value):
    key = "relay_recent_measurement_attempt_count"
    if value > config.MAX_RECENT_PRIORITY_LIST_COUNT:
        logger.warning(
            "%s (%s) is greater than the maximum.",
            key,
            value,
        )


def validate_relay_recent_measurement_failure_count_max(value):
    key = "relay_recent_measurement_failure_count"
    if value > config.MAX_RECENT_PRIORITY_LIST_COUNT:
        logger.warning(
            "%s (%s) is greater than the maximum.",
            key,
            value,
        )


def validate_relay_recent_measurements_excluded_error_count_max(value):
    key = "relay_recent_measurements_excluded_error_count"
    if value > config.MAX_RECENT_PRIORITY_LIST_COUNT:
        logger.warning(
            "%s (%s) is greater than the maximum.",
            key,
            value,
        )


def validate_recent_keyvalues(d):
    """Check that the different `recent` KeyValues are coherent with each other

    The correct values should be:
    #consensus >= #priority relays >= #attempts >= #errors >= #failures.

    The last one is likely to be false when the scanner is cancelled after a
    short time running (the attempts would canceled but not finish with an
    error).

    """
    # logger.debug("Validating recent KeyValues.")
    key1 = "relay_in_recent_consensus_count"
    value1 = d.get(key1, 0)
    key2 = "relay_in_recent_priority_list_count"
    value2 = d.get(key1, 0)
    if value1 < value2:
        util.validate_recent_log(key1, value1, key2, value2)

    key1 = "relay_recent_priority_relay_count"
    value1 = d.get(key1, 0)
    key2 = "relay_recent_measurement_attempt_count"
    value2 = d.get(key1, 0)
    if value1 < value2:
        util.validate_recent_log(key1, value1, key2, value2)

    key1 = "relay_recent_measurement_attempt_countt"
    value1 = d.get(key1, 0)
    key2 = "relay_recent_measurements_excluded_error_count"
    value2 = d.get(key1, 0)
    if value1 < value2:
        util.validate_recent_log(key1, value1, key2, value2)

    key1 = "relay_recent_measurements_excluded_error_count"
    value1 = d.get(key1, 0)
    key2 = "relay_recent_measurements_excluded_failure_count"
    value2 = d.get(key1, 0)
    if value1 < value2:
        util.validate_recent_log(key1, value1, key2, value2)
