# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.contrib import admin

from onbasca.onbasca.models.webserver import WebServer


@admin.register(WebServer)
class WebServerAdmin(admin.ModelAdmin):
    list_display = ("url", "enabled", "verify")
