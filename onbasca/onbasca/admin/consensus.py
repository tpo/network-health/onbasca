# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.contrib import admin

from onbasca.onbasca.models.consensus import Consensus


@admin.register(Consensus)
class ConsensusAdmin(admin.ModelAdmin):
    list_display = (
        "valid_after",
        "routerstatuses_count",
        "_obj_created_at",
        "weight_sum",
    )
    ordering = ("-valid_after",)
    list_filter = ["valid_after"]
