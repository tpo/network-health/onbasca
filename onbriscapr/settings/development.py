# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""Django development settings for onbriscapr project."""
from os import environ

from .base import *  # noqa: F403


DEBUG = True
ALLOWED_HOSTS = ["*"]
INSTALLED_APPS.extend([
    # For django admin app
    "django.contrib.admin",
    "django.contrib.messages",
    "django.contrib.sessions",
    "django.contrib.staticfiles",
    # For tests
    "django_extensions",
    "factory_generator",
])

# For django admin
MIDDLEWARE.extend([
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
])

# For django admin
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

STATIC_URL = "/static/"

SECRET_KEY = environ.get("SECRET_KEY", "django-insecure-c7koj=4z7wh0g--^h@8_#e34dabvzr&5*rlp9mttkw@$bi*^0w")

# shell_plus
SHELL_PLUS = "ipython"
# print SQL queries in shell_plus
SHELL_PLUS_PRINT_SQL = True

FACTORY_FIELD_FAKER_MAP = {
    "PositiveBigIntegerField": "onbasca.onbasca.factories.fields_faker.PositiveBigIntegerFieldFaker",
}
# FACTORY_ROOT_DIR = os.path.join(BASE_DIR, "..", "onbrisca", "factories")

try:
    from .local import *
except ImportError:
    print("Missing local.py in settings.")
