# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""onbriscapr URL Configuration"""
from django.urls import include, path
from django.conf import settings

urlpatterns = [
    path("", include("onbrisca.urls")),
]
if settings.DEBUG:
    print("urls: DEBUG is True")
    from django.contrib import admin

    urlpatterns.append(
        path("admin/", admin.site.urls),
    )
